#include "ImageInterpolation.h"
#include "ColorSpaces.h"
#include <math.h>

void sampleAndHold(const uchar input[], int xSize, int ySize, uchar output[], int newXSize, int newYSize)
{
    int iX, iY;
    int origX, origY;

	uchar *Yin = new uchar[xSize * ySize];
	char *Uin = new char[xSize * ySize / 4];
	char *Vin = new char[xSize * ySize / 4];

	uchar *Yout = new uchar[newXSize * newYSize];
	char *Uout = new char[newXSize * newYSize / 4];
	char *Vout = new char[newXSize * newYSize / 4];

    double FH = (double)newXSize / xSize;
    double FV = (double)newYSize / ySize;

    RGBtoYUV420(input, xSize, ySize, Yin, Uin, Vin);


    for (iY = 0; iY < newYSize; iY++) {
        for (iX = 0; iX < newXSize; iX++) {
			origX = round((double)(iX - 1) / FH + 1);
			origY = round((double)(iY - 1) / FV + 1);

            Yout[iY * newXSize + iX] = Yin[origY * xSize + origX];

            // FUTURE: 4 calculations instead of 1. Condition can fix it
           Uout[(iY / 2) * newXSize / 2 + iX / 2] = Uin[(origY / 2) * xSize / 2 + origX / 2];
           Vout[(iY / 2) * newXSize / 2 + iX / 2] = Vin[(origY / 2) * xSize / 2 + origX / 2];
        }
    }

    YUV420toRGB(Yout, Uout, Vout, newXSize, newYSize, output);

	delete[] Yin;
	delete[] Uin;
	delete[] Vin;
	delete[] Yout;
	delete[] Uout;
	delete[] Vout;
}

void bilinearInterpolate(const uchar input[], int xSize, int ySize, uchar output[], int newXSize, int newYSize)
{
    int iX, iY;
    double a, b;
    int m, n;

    uchar *Yin = new uchar[xSize * ySize];
    char *Uin = new char[xSize * ySize / 4];
    char *Vin = new char[xSize * ySize / 4];

    uchar *Yout = new uchar[newXSize * newYSize];
    char *Uout = new char[newXSize * newYSize / 4];
    char *Vout = new char[newXSize * newYSize / 4];

    double Sh = (double)newXSize / xSize;
    double Sv = (double)newYSize / ySize;

    RGBtoYUV420(input, xSize, ySize, Yin, Uin, Vin);

    for (iY = 0; iY < newYSize; iY++) {
        for (iX = 0; iX < newXSize; iX++) {
                // ns -> iX, ms -> iY
                a = iX / Sh - floor(iX / Sh);
                b = iY / Sv - floor(iY / Sv);

                m = floor(iX / Sh);
                n = floor(iY / Sv);

                Yout[iY * newXSize + iX] = (1 - a) * (1 - b) * Yin[n * xSize + m] +
                        (1 - a) * b * Yin[(n + 1) * xSize + m] +
                        a * (1 - b) * Yin[n * xSize + m + 1] +
                        a * b * Yin[(n + 1) * xSize + m + 1];

				Uout[(iY / 2) * newXSize / 2 + iX / 2] = (1 - a) * (1 - b) * Uin[(n / 2) * xSize / 2 + m / 2] +
					(1 - a) * b * Uin[((n + 1) / 2) * xSize / 2 + m / 2] +
					a * (1 - b) * Uin[(n / 2) * xSize / 2 + (m + 1) / 2] +
					a * b * Uin[((n + 1) / 2) * xSize / 2 + (m + 1) / 2];

				Vout[(iY / 2) * newXSize / 2 + iX / 2] = (1 - a) * (1 - b) * Vin[(n / 2) * xSize / 2 + m / 2] +
					(1 - a) * b * Vin[((n + 1) / 2) * xSize / 2 + m / 2] +
					a * (1 - b) * Vin[(n / 2) * xSize / 2 + (m + 1) / 2] +
					a * b * Vin[((n + 1) / 2) * xSize / 2 + (m + 1) / 2];
            }
    }

    YUV420toRGB(Yout, Uout, Vout, newXSize, newYSize, output);

    delete[] Yin;
    delete[] Uin;
    delete[] Vin;
    delete[] Yout;
    delete[] Uout;
    delete[] Vout;
}


void imageRotate(const uchar input[], int xSize, int ySize, uchar output[], int m, int n, double angle)
{
    int iX, iY;

    uchar *Yin = new uchar[xSize * ySize];
    char *Uin = new char[xSize * ySize / 4];
    char *Vin = new char[xSize * ySize / 4];

    uchar *Yout = new uchar[xSize * ySize];
    char *Uout = new char[xSize * ySize / 4];
    char *Vout = new char[xSize * ySize / 4];


    RGBtoYUV420(input, xSize, ySize, Yin, Uin, Vin);


    for (iY = 0; iY < ySize; iY++) {
        for (iX = 0; iX < xSize; iX++) {
            int xPrim = iX * cos(angle) - iY * sin(angle) - m * cos(angle) + n * sin(angle) + m;
            int yPrim = iY * cos(angle) + iX * sin(angle) - m * sin(angle) - n * cos(angle) + n;

            if (xPrim < 0 || xPrim >= xSize || yPrim < 0 || yPrim > ySize) {
                Yout[iY * xSize + iX] = 0;
                Uout[(iY / 2) * xSize / 2 + iX / 2] = 0;
                Vout[(iY / 2) * xSize / 2 + iX / 2] = 0;
            } else {
                Yout[iY * xSize + iX] = Yin[yPrim * xSize + xPrim];

				if (iY % 2 == 1 && iX % 2 == 1) {
					Uout[(iY / 2) * xSize / 2 + iX / 2] = Uin[(yPrim / 2) * xSize / 2 + xPrim / 2];
					Vout[(iY / 2) * xSize / 2 + iX / 2] = Vin[(yPrim / 2) * xSize / 2 + xPrim / 2];
				}
            }
        }
    }

    YUV420toRGB(Yout, Uout, Vout, xSize, ySize, output);

    delete[] Yin;
    delete[] Uin;
    delete[] Vin;
    delete[] Yout;
    delete[] Uout;
    delete[] Vout;
}

void imageRotateBilinear(const uchar input[], int xSize, int ySize, uchar output[], int m, int n, double angle)
{
    int iX, iY;

    uchar *Yin = new uchar[xSize * ySize];
    char *Uin = new char[xSize * ySize / 4];
    char *Vin = new char[xSize * ySize / 4];

    uchar *Yout = new uchar[xSize * ySize];
    char *Uout = new char[xSize * ySize / 4];
    char *Vout = new char[xSize * ySize / 4];


    RGBtoYUV420(input, xSize, ySize, Yin, Uin, Vin);



    for (iY = 0; iY < ySize; iY++) {
        for (iX = 0; iX < xSize; iX++) {
            double xPrim = iX * cos(angle) - iY * sin(angle) - m * cos(angle) + n * sin(angle) + m;
            double yPrim = iY * cos(angle) + iX * sin(angle) - m * sin(angle) - n * cos(angle) + n;

			double a = xPrim - floor(xPrim);
			double b = yPrim - floor(yPrim);

			int m = (int)xPrim;
			int n = (int)yPrim;

            if (xPrim < 0 || xPrim > xSize || yPrim < 0 || yPrim > ySize) {
                Yout[iY * xSize + iX] = 0;
                Uout[(iY / 2) * xSize / 2 + iX / 2] = 0;
                Vout[(iY / 2) * xSize / 2 + iX / 2] = 0;
            } else {
                Yout[iY * xSize + iX] = (1 - a) * (1 - b) *  Yin[n * xSize + m] +
					(1 - a) * b * Yin[(n + 1) * xSize + m] +
					a * (1 - b) * Yin[n * xSize + m + 1] +
					a * b * Yin[(n + 1) * xSize + m + 1];
   

				if (iY % 2 == 0 && iX % 2 == 0) {
					Uout[(iY / 2) * xSize / 2 + iX / 2] = (1 - a) * (1 - b) * Uin[(n / 2) * xSize / 2 + m / 2] +
						(1 - a) * b * Uin[((n + 1) / 2) * xSize / 2 + m / 2] +
						a * (1 - b) * Uin[(n / 2) * xSize / 2 + (m + 1) / 2] +
						a * b * Uin[((n + 1) / 2) * xSize / 2 + (m + 1) / 2];

					Vout[(iY / 2) * xSize / 2 + iX / 2] = (1 - a) * (1 - b) * Vin[(n / 2) * xSize / 2 + m / 2] +
						(1 - a) * b * Vin[((n + 1) / 2) * xSize / 2 + m / 2] +
						a * (1 - b) * Vin[(n / 2) * xSize / 2 + (m + 1) / 2] +
						a * b * Vin[((n + 1) / 2) * xSize / 2 + (m + 1) / 2];
				}
            }
        }
    }

    YUV420toRGB(Yout, Uout, Vout, xSize, ySize, output);

    delete[] Yin;
    delete[] Uin;
    delete[] Vin;
    delete[] Yout;
    delete[] Uout;
    delete[] Vout;
}
