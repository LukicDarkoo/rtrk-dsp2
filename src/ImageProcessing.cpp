
#include "ImageProcessing.h"
#include "ImageInterpolation.h"
#include <QDebug>

#define _USE_MATH_DEFINES
#include <math.h>



void imageProcessingFun(const QString& progName, QImage* const outImgs, const QImage* const inImgs, const QVector<double>& params) 
{
	int X_SIZE = inImgs->width();
	int Y_SIZE = inImgs->height();

	/* NOTE: Calculate output image resolution and construct output image object */

	if(progName == "Sample and hold") 
	{	
        /* Input image data in RGB format can be obtained with inImgs->bits() */
        /* Vertical scale factor is params[0] */
        /* Horizontal scale factor is params[1] */

        /* TO DO: Calculate output image resolution and construct output image object */

        /* TO DO: Perform Sample and hold interpolation  */

        qInfo("Sample and hold");

        int NEW_X_SIZE = params[1] * X_SIZE;
        qInfo() << NEW_X_SIZE;
        int NEW_Y_SIZE = params[0] * Y_SIZE;

		while (NEW_X_SIZE % 4 != 0) NEW_X_SIZE++;
		while (NEW_Y_SIZE % 4 != 0) NEW_Y_SIZE++;

        *outImgs = *(new QImage(NEW_X_SIZE, NEW_Y_SIZE, inImgs->format()));
        sampleAndHold(inImgs->bits(), X_SIZE, Y_SIZE, outImgs->bits(), NEW_X_SIZE, NEW_Y_SIZE);
	}
	else if (progName == "Bilinear") 
	{
		/* Input image data in RGB format can be obtained with inImgs->bits() */
		/* Vertical scale factor is params[0] */
		/* Horizontal scale factor is params[1] */

		/* TO DO: Calculate output image resolution and construct output image object */

		/* TO DO: Perform Bilinear interpolation  */

        int NEW_X_SIZE = params[1] * X_SIZE;
        qInfo() << NEW_X_SIZE;
        int NEW_Y_SIZE = params[0] * Y_SIZE;

		while (NEW_X_SIZE % 4 != 0) NEW_X_SIZE++;
		while (NEW_Y_SIZE % 4 != 0) NEW_Y_SIZE++;

        *outImgs = *(new QImage(NEW_X_SIZE, NEW_Y_SIZE, inImgs->format()));
        bilinearInterpolate(inImgs->bits(), X_SIZE, Y_SIZE, outImgs->bits(), NEW_X_SIZE, NEW_Y_SIZE);
	}
	else if(progName == "Rotation") 
	{	
		/* Input image data in RGB format can be obtained with inImgs->bits() */
		/* Rotation angle in degrees is params[0]*/
		/* Center of rotation coordinates are (XSIZE/2, YSIZE/2) */

		/* TO DO: Construct output image object */

		/* TO DO: Perform image rotation */

        double angle = (params[0] * M_PI) / 180;
         *outImgs = *(new QImage(X_SIZE, Y_SIZE, inImgs->format()));
        imageRotate(inImgs->bits(), X_SIZE, Y_SIZE, outImgs->bits(), X_SIZE / 2, Y_SIZE / 2, angle);
	
	}
	else if (progName == "Rotation Bilinear") 
	{
		/* Input image data in RGB format can be obtained with inImgs->bits() */
		/* Rotation angle in degrees is params[0]*/
		/* Center of rotation coordinates are (XSIZE/2, YSIZE/2) */

		/* TO DO: Construct output image object */

		/* TO DO: Perform image rotation with bilinear interpolation */

        double angle = (params[0] * M_PI) / 180;
         *outImgs = *(new QImage(X_SIZE, Y_SIZE, inImgs->format()));
        imageRotateBilinear(inImgs->bits(), X_SIZE, Y_SIZE, outImgs->bits(), X_SIZE / 2, Y_SIZE / 2, angle);
	}

}

